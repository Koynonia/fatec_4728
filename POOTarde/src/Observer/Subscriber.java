package Observer;

public interface Subscriber {
	
	public void updates(String msg);
}
